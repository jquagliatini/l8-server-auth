const { config } = require('dotenv');

config();

const { createServer } = require('restify');
const logger = require('signale');

const makeServer = require('./src/makeServer.js');
const checkEnv = require('./src/utils/checkEnv.js');

if (!checkEnv()) {
  throw new Error('Missing mandatory envvar');
}

const server = makeServer(
  createServer({
    name: 'l8-auth',
  }),
);

server.listen(process.env.APP_PORT || 3000, () => {
  logger.info(`server listens on ${process.env.APP_PORT || 3000}`);
});
