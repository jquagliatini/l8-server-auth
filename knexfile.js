const baseConfig = {
  client: 'sqlite3',
  useNullAsDefault: true,
  migrations: {
    directory: './src/models/migrations',
  },
};

module.exports = {
  development: {
    ...baseConfig,
    connection: {
      filename: 'l8-auth.sqlite',
    },
  },
  test: {
    ...baseConfig,
    connection: {
      filename: 'l8-auth.test.sqlite',
    },
  },

};
