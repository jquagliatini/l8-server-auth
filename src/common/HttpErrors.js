const {
  BadRequestError,
  ConflictError,
  ForbiddenError,
  InternalServerError,
} = require('restify-errors');

module.exports = {
  basicAuthExpected() {
    return new BadRequestError('Basic Authentication Expected');
  },

  cannotAccessRsc() {
    return new ForbiddenError('You cannot access this resource');
  },

  couldNotProcessReq() {
    return new InternalServerError('Could not process the request')
  },

  duplicateError() {
    return new ConflictError('Duplicate user');
  },

  forbiddenError() {
    return new ForbiddenError('Wrong username or password');
  },

  missingField() {
    return new BadRequestError('Missing mandatory input field');
  }
};
