const routePaths = {
  SIGNUP: '/signup',
  SIGNIN: '/signin',
  CHECKJWT: '/u/_check',
  HEALTH: '/_health',
};

module.exports = {
  routePaths,
};
