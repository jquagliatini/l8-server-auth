module.exports = {
  health(_, res, next) {
    res.send(204);
    return next();
  },
};
