const logger = require('signale');

const Users = require('../models/Users.model.js');
const Tokens = require('../models/Tokens.model.js');
const {
  cannotAccessRsc,
  couldNotProcessReq,
  duplicateError,
  forbiddenError,
} = require('../common/HttpErrors.js');

module.exports = {
  signup(req, res, next) {
    if (req.user) {
      return next(duplicateError());
    }

    return Users.create({
      email: req.authorization.basic.username,
      password: req.authorization.basic.password,
    })
      .then(user => {
        res.status(201);
        res.json({
          status: 201,
          data: {
            ...user,
            created_at: new Date(user.created_at).toISOString(),
          },
        });
        return next();
      })
      .catch(e => {
        logger.warn(e);
        return next(
          e.code === 'DUPLICATE' ? duplicateError() : couldNotProcessReq(),
        );
      });
  },

  signin(req, res, next) {
    if (!req.user) {
      return next(forbiddenError());
    }

    res.json(
      Tokens.create({
        email: req.user.email,
      }),
    );
    return next();
  },

  checkAuth(req, res, next) {
    if (!req.username) {
      return next(cannotAccessRsc());
    }

    res.send(204);
    return next();
  },
};
