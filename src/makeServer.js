const routes = require('./routes/index.js');

module.exports = server => routes.reduce((s, route) => route(s), server);
