const { basicAuthExpected, missingField } = require('../common/HttpErrors.js');
const Users = require('../models/Users.model.js');

module.exports = function BasicAuthMiddleware(req, _, next) {
  if (
    !req.authorization ||
    req.authorization.scheme !== 'Basic' ||
    !req.authorization.basic
  ) {
    return next(basicAuthExpected());
  }

  const {
    username = '@NOUSERNAME',
    password = '@NOPWD',
  } = req.authorization.basic;

  if (
    ['@NOUSERNAME', null].includes(username) ||
    ['@NOPWD', null].includes(password)
  ) {
    return next(missingField());
  }

  Users.findByEmailPassword({
    email: username,
    password,
  }).then(user => {
    req.user = user;
    return next();
  });
};
