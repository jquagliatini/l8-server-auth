const { cannotAccessRsc } = require('../common/HttpErrors.js');
const Tokens = require('../models/Tokens.model.js');

module.exports = function JwtBearerMiddleware(req, _, next) {
  if (!req.authorization || req.authorization.scheme !== 'Bearer') {
    return next(cannotAccessRsc());
  }

  Tokens.validate(req.authorization.credentials).then(
    decoded => {
      req.username = decoded.email;
      return next();
    },
    () => next(cannotAccessRsc()),
  );
};
