const jwt = require('jsonwebtoken');
const logger = require('signale');

const db = require('./db.js');

const invalidToken = () => new Error('invalid token');

module.exports = {
  create(payload) {
    const iat = new Date();
    const token = jwt.sign(
      { ...payload, iat: Math.ceil(iat.getTime() / 1000) },
      process.env.JWT_SECRET,
      { expiresIn: '1h' },
    );

    const tokenToSave = {
      issued_at: iat,
      expires_at: new Date(iat.getTime() + 3600 * 1000),
      token,
    };

    db('tokens')
      .insert({ ...tokenToSave, valid: true })
      .catch(e => {
        logger.warn(`Failed inserting token: ${e}`);
      });

    return tokenToSave;
  },

  validate(token) {
    return new Promise((resolve, reject) => {
      jwt.verify(
        token,
        process.env.JWT_SECRET,
        (err, decoded) => (err ? reject(invalidToken()) : resolve(decoded)),
      );
    })
      .then(decoded =>
        Promise.all([
          decoded,
          db('tokens')
            .where({ token })
            .first('expires_at', 'valid'),
        ]),
      )
      .then(([decoded, t]) => {
        if (!t || (t && !t.valid)) {
          return Promise.reject(invalidToken());
        }

        if (t.expires_at < Date.now()) {
          db('tokens')
            .where({ token })
            .del();
          return Promise.reject(invalidToken());
        }

        return decoded;
      });
  },
};
