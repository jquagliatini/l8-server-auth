const bcrypt = require('bcryptjs');

const db = require('./db.js');

const SQLITE_DUPLICATE_CODE = 19;

module.exports = {
  /** @return {Promise<{ email: string, created_at: Date }>} */
  create({ email, password }) {
    return bcrypt
      .hash(password, 10)
      .then(hash =>
        db('users').insert({
          created_at: new Date(),
          password: hash,
          email,
        }),
      )
      .then(
        () =>
          db('users')
            .first('email', 'created_at')
            .where({
              email,
            }),
        e => {
          throw e.code === SQLITE_DUPLICATE_CODE
            ? Object.assign(new Error('Duplicate User'), { code: 'DUPLICATE' })
            : e;
        },
      );
  },

  /** @return {Promise<null | { email: string, password: string }>} */
  findByEmail({ email }) {
    return db('users')
      .first('email', 'password')
      .where({
        email,
      });
  },

  /** @return {Promise<null | { email: string, password: string }>} */
  findByEmailPassword({ email, password }) {
    return this.findByEmail({ email }).then(
      user =>
        user
          ? bcrypt
              .compare(password, user.password)
              .then(ok => (ok ? user : null))
          : null,
    );
  },
};
