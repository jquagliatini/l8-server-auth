const knex = require('knex');
const dbConfig = require('../../knexfile.js');

module.exports = table => {
  const dbConnection = knex(dbConfig[process.env.NODE_ENV || 'development']);
  return table ? dbConnection(table) : dbConnection;
};
