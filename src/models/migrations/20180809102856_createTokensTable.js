exports.up = knex =>
  knex.schema.createTable('tokens', t => {
    t.string('token', 512)
      .primary()
      .unique()
      .notNullable();
    t.dateTime('issued_at').notNullable();
    t.dateTime('expires_at').notNullable();
    t.boolean('valid').notNullable();
  });

exports.down = knex => knex.schema.dropTable('tokens');
