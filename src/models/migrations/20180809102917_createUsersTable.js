const createUsersTable = knex =>
  knex.schema.createTable('users', t => {
    t.string('email')
      .primary()
      .notNullable()
      .unique();
    t.string('password').notNullable();
    t.dateTime('created_at').notNullable();
    t.dateTime('modified_at').nullable();
  });

exports.up = createUsersTable;

exports.down = knex => knex.schema.dropTable('users');
