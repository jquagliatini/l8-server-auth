const TOKEN_IDX = 'IDX_TOKEN_EXP';

exports.up = knex =>
  knex.schema.table('tokens', t => {
    t.index(['expires_at'], TOKEN_IDX);
  });

exports.down = knex =>
  knex.schema.table('tokens', t => {
    t.dropIndex(['expires_at'], TOKEN_IDX);
  });
