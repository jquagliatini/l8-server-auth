const cors = require('cors');

const HealthController = require('../controllers/HealthController.js');
const route = require('../utils/route.js');

module.exports = server => {
  const options = {
    methods: ['GET,POST,PUT,PATCH,OPTIONS'],
  };

  return ['get', 'post', 'put', 'patch', 'opts'].reduce((s, method) => {
    s[method].apply(s, [
      route(
        method === 'opts'
          ? method.slice(0, -1).toUpperCase()
          : method.toUpperCase(),
        'HEALTH',
      ),
      cors(options),
      HealthController.health,
    ]);
    return s;
  }, server);
};
