const usersRoute = require('./users.route.js');
const healthRoute = require('./health.route.js');

module.exports = [usersRoute, healthRoute];
