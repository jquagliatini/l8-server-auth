const {
  plugins: { authorizationParser },
} = require('restify');
const cors = require('cors');

const BasicAuthMiddleware = require('../middlewares/BasicAuthMiddleware.js');
const JwtBearerMiddleware = require('../middlewares/JwtBearerMiddleware.js');
const UsersController = require('../controllers/UsersController.js');
const route = require('../utils/route.js');

module.exports = server => {
  const opts = {
    methods: 'POST,OPTIONS',
  };

  ['signup', 'signin'].forEach(routeName => {
    server.opts(route('OPT', routeName.toUpperCase()), cors(opts));
    server.post(
      route('POST', routeName.toUpperCase()),
      cors(opts),
      authorizationParser(),
      BasicAuthMiddleware,
      UsersController[routeName],
    );
  });

  const checkjwtCorsOpts = { methods: ['GET', 'POST', 'OPTIONS'] };

  server.opts(route('OPT', 'CHECKJWT'), cors(checkjwtCorsOpts));
  ['GET', 'POST'].forEach(verb => {
    server[verb.toLowerCase()](
      route(verb, 'CHECKJWT'),
      cors(checkjwtCorsOpts),
      authorizationParser(),
      JwtBearerMiddleware,
      UsersController.checkAuth,
    );
  });

  return server;
};
