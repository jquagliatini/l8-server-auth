module.exports = function checkEnv() {
  const env = Object.keys(process.env);
  return [
    'JWT_SECRET'
  ].every(k => env.includes(k));
}
