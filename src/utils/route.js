const { routePaths } = require('../common/constants.js');

module.exports = function route(method, name) {
  if (!Object.prototype.hasOwnProperty.call(routePaths, name)) {
    throw new Error(`${name} is not a valid property`);
  }

  return {
    name: `${method.toLowerCase()}_${name.toLowerCase()}`,
    path: routePaths[name],
  };
};
