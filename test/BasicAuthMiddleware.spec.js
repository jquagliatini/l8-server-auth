/* eslint-env jest */
const { BadRequestError } = require('restify-errors');
const bcrypt = require('bcryptjs');

const BasicAuthMiddleware = require('../src/middlewares/BasicAuthMiddleware.js');
const db = require('../src/models/db.js');

const cb = done => e => {
  expect(e).toBeInstanceOf(BadRequestError);
  done();
};

describe('BasicAuthMiddleware', () => {
  it('should return an error if req.authorization is missing', done => {
    BasicAuthMiddleware({}, null, cb(done));
  });

  it('should return an error if auth.scheme is !Basic', done => {
    BasicAuthMiddleware(
      {
        authorization: {
          scheme: 'WRONG',
        },
      },
      null,
      cb(done),
    );
  });

  it('should return an error if auth.basic is missing', done => {
    BasicAuthMiddleware(
      {
        authorization: {
          scheme: 'Basic',
        },
      },
      null,
      cb(done),
    );
  });

  it('should return an error if auth.basic.password is missing', done => {
    BasicAuthMiddleware(
      {
        authorization: {
          scheme: 'Basic',
          basic: {
            username: 'john.doe',
          },
        },
      },
      null,
      cb(done),
    );
  });

  it('should return an error if auth.basic.username is missing', done => {
    BasicAuthMiddleware(
      {
        authorization: {
          scheme: 'Basic',
          basic: {
            password: 'pwd',
          },
        },
      },
      null,
      cb(done),
    );
  });

  describe('succesul authentication', () => {
    beforeAll(async () => {
      await db().migrate.rollback();
      await db().migrate.latest();
    });

    afterAll(async () => {
      await db().migrate.rollback();
    });

    it('should find a user in the database', done => {
      const user = {
        email: 'john.doe@example.com',
        password: 'password',
        created_at: new Date(),
      };

      bcrypt
        .hash(user.password, 10)
        .then(hash => db('users').insert({ ...user, password: hash }))
        .then(() => {
          const req = {
            authorization: {
              scheme: 'Basic',
              basic: {
                username: user.email,
                password: user.password,
              },
            },
          };

          BasicAuthMiddleware(req, null, () => {
            expect(Object.keys(req.user)).toEqual(['email', 'password']);
            expect(req.user.email).toBe(user.email);
            done();
          });
        })
        .catch(done);
    });
  });
});
