/* eslint-env jest */
const db = require('../src/models/db.js');

describe('db', () => {
  beforeAll(async () => {
    await db().migrate.rollback();
    await db().migrate.latest();
  });

  afterAll(async () => db().migrate.rollback());

  it('should create a user', () =>
    db('users')
      .insert({
        email: 'john.doe@example.com',
        password: 'p4ssw0rd!',
        created_at: new Date(),
      })
      .then(() =>
        db('users')
          .count({
            count: 'email',
          })
          .then(([{ count }]) => {
            expect(count).toBe(1);
          }),
      ));
});
