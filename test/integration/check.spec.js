/* eslint-env jest */
const request = require('supertest');
const { createServer } = require('restify');
const jsonwebtoken = require('jsonwebtoken');

const db = require('../../src/models/db.js');
const makeServer = require('../../src/makeServer.js');

const server = makeServer(createServer());

describe('[integration] jwt checking scenario', () => {
  let validJwt;
  let expiredJwt;
  let invalidJwt;
  beforeAll(async () => {
    process.env.JWT_SECRET = 'secret';
    await db().migrate.rollback();
    await db().migrate.latest();

    const iat = new Date();
    validJwt = jsonwebtoken.sign(
      { email: 'john.doe@example.com', iat: Math.ceil(iat.getTime() / 1000) },
      process.env.JWT_SECRET,
      { expiresIn: '1h' },
    );

    expiredJwt = jsonwebtoken.sign(
      {
        email: 'john.doe@example.com',
        iat: Math.ceil((iat.getTime() - 3600 * 1000) / 1000),
        exp: Math.ceil((iat.getTime() - 3000) / 1000),
      },
      process.env.JWT_SECRET,
    );

    invalidJwt = jsonwebtoken.sign(
      {
        email: 'jane.doe@example.com',
        iat: Math.ceil(iat.getTime() / 1000),
      },
      process.env.JWT_SECRET,
      { expiresIn: '1h' },
    );

    return db('tokens').insert([
      {
        token: validJwt,
        expires_at: new Date(iat.getTime() + 3600 * 1000),
        issued_at: iat,
        valid: true,
      },
      {
        token: expiredJwt,
        expires_at: new Date(iat.getTime() - 3000),
        issued_at: new Date(iat.getTime() - 3600 * 1000),
        valid: true,
      },
      {
        token: invalidJwt,
        expires_at: new Date(iat.getTime() + 3600 * 1000),
        issued_at: iat,
        valid: false,
      },
    ]);
  });

  afterAll(async () => db().migrate.rollback());

  ['get', 'post'].forEach(method => {
    test(`[${method.toUpperCase()}] /u/_check : should validate a valid JWT`, () =>
      request(server)
        [method]('/u/_check')
        .set('Authorization', `Bearer ${validJwt}`)
        .expect(204));
  });

  test('should forbid for expired JWT', () =>
    request(server)
      .get('/u/_check')
      .set('Authorization', `Bearer ${expiredJwt}`)
      .expect(403));

  test('should forbid for invalid JWT', () =>
    request(server)
      .get('/u/_check')
      .set('Authorization', `Bearer ${invalidJwt}`)
      .expect(403));
});
