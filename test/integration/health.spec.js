/* eslint-env jest */
const supertest = require('supertest');
const { createServer } = require('restify')

const makeServer = require('../../src/makeServer.js');

const server = makeServer(createServer());

const request = supertest.agent(server);

describe('[integration] health check', () => {
  afterAll(() => server.close());

  ['get', 'post', 'put', 'patch', 'options'].forEach(method =>
    test(`[${method.toUpperCase()}] /_health`, () =>
      request[method].apply(request, ['/_health']).expect(204)),
  );
});
