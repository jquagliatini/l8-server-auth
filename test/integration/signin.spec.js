/* eslint-env jest */
const request = require('supertest');
const { createServer } = require('restify')

const bcrypt = require('bcryptjs');
const makeServer = require('../../src/makeServer.js');
const db = require('../../src/models/db.js');

const server = makeServer(createServer());

describe('[integration] signin scenario', () => {
  const johnDoe = {
    email: 'john.doe@example.com',
    password: 'p4sSw0rd!',
  };

  beforeAll(async () => {
    process.env.JWT_SECRET = 'secret';

    await db().migrate.rollback();
    await db().migrate.latest();

    await bcrypt.hash(johnDoe.password, 10).then(hash =>
      db('users').insert({
        created_at: new Date(),
        password: hash,
        email: johnDoe.email,
      }),
    );
  });

  afterAll(async () => {
    await db().migrate.rollback();
  });

  it('should signin', () =>
    request(server)
      .post('/signin')
      .auth(johnDoe.email, johnDoe.password)
      .expect(200)
      .expect('Content-Type', /json/)
      .then(res => {
        expect(Object.keys(res.body)).toEqual([
          'issued_at',
          'expires_at',
          'token',
        ]);
      }));
});
