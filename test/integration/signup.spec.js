/* eslint-env jest */
const request = require('supertest');
const { createServer } = require('restify');

const makeServer = require('../../src/makeServer.js');
const db = require('../../src/models/db.js');

const server = makeServer(createServer());

describe('[integration] signup scenario', () => {
  const johnDoe = {
    email: 'john.doe@example.com',
    password: 'p4sSw0rd!',
  };

  beforeAll(async () => {
    await db().migrate.rollback();
    await db().migrate.latest();
  });

  afterAll(async () => {
    await db().migrate.rollback();
  });

  it('should signup using `Basic Auth`', () =>
    request(server)
      .post('/signup')
      .auth(johnDoe.email, johnDoe.password)
      .expect(201)
      .expect('Content-Type', /json/)
      .then(res => {
        expect(res.body.status).toBe(201);
        expect(res.body.data.email).toBe(johnDoe.email);
        expect(Object.keys(res.body.data)).toEqual(['email', 'created_at']);
      }));

  it('should 409 when trying to signup existing email', () =>
    request(server)
      .post('/signup')
      .auth(johnDoe.email, johnDoe.password)
      .expect(409));

  it('should 400 when no Basic Auth provided', () =>
    request(server)
      .post('/signup')
      .expect(400));

  it('should 400 when missing password', () =>
    request(server)
      .post('/signup')
      .set(
        'Authorization',
        `Basic ${Buffer.from(johnDoe.email).toString('base64')}`,
      )
      .expect(400));

  it('should 400 when missing username', () =>
    request(server)
      .post('/signup')
      .set(
        'Authorization',
        `Basic ${Buffer.from(johnDoe.password).toString('base64')}`,
      )
      .expect(400));
});
