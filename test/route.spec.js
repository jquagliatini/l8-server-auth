/* eslint-env jest */
const route = require('../src/utils/route.js');
const { routePaths } = require('../src/common/constants.js');

describe('route', () => {
  test('existing name', () => {
    expect(route('POST', 'SIGNIN')).toEqual({
      name: `post_signin`,
      path: routePaths.SIGNIN,
    });
  });

  test('wrong name', () => {
    const cb = () => route('POST', 'WRONG');
    expect(cb).toThrow('WRONG is not a valid property');
  });
});
